use std::env;
use std::fs;
use std::process;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let tree_map = make_map(file_content);
    let visible = find_visible(&tree_map);
    let visible_count = count_visible(&visible);
    println!("Solution 1 is {}", visible_count);
    let scene = find_scenic(&tree_map);
    let highest_scenic_score = find_max(&scene);
    println!("Solution 2 is {}", highest_scenic_score);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn make_map(file_string: String) ->  Vec<Vec<u32>> {
    let lines: Vec<&str> = file_string.split("\n").collect();
    let mut height_map: Vec<Vec<u32>> = Vec::new();
    let y_len = lines.len();
    for i in 0..y_len {
        let line_content = lines[i].chars().map(|x| x.to_digit(10).unwrap());
        height_map.push(Vec::new());
        for num in line_content {
            height_map[i].push(num);
        }
    }
    return height_map;
}

fn find_visible(map: &Vec<Vec<u32>>) ->  Vec<Vec<u32>> {
    let mut vis_map: Vec<Vec<u32>> = Vec::new();
    let vert_len = map.len();
    for i in 0..vert_len {
        vis_map.push(vec![0; vert_len]);
        let horiz_len = map[i].len();
        for j in 0..horiz_len {
            if i == 0 || j == 0 {
                vis_map[i][j] = 1;
            } else if i == vert_len - 1 || j == horiz_len - 1 {
                vis_map[i][j] = 1;
            } else {
                let right_max = &map[i][0..j]
                    .iter().max().unwrap();
                let left_max = &map[i][j+1..horiz_len]
                    .iter().max().unwrap();
                let mut vert_vec: Vec<u32> = Vec::new();
                for k in 0..vert_len {
                    vert_vec.push(map[k][j]);
                }
                let top_max = &vert_vec[0..i]
                    .iter().max().unwrap();
                let bottom_max = &vert_vec[i+1..vert_len]
                    .iter().max().unwrap();
                let min_vec: Vec<u32> = vec![
                    **right_max, **left_max, **top_max, **bottom_max];
                for this_max in min_vec {
                    if this_max < map[i][j] {
                        vis_map[i][j] = 1;
                        break;
                    }
                }
            }
        }
    }
    return vis_map;
}

fn count_visible(map: &Vec<Vec<u32>>) -> u32 {
    let mut count = 0;
    for i in 0..map.len() {
        for j in 0..map[i].len() {
            count += map[i][j]
        }
    }
    return count;
}

fn find_scenic(map: &Vec<Vec<u32>>) -> Vec<Vec<u32>> {
    let mut scene_map: Vec<Vec<u32>> = Vec::new();
    let vert_len = map.len();
    for i in 0..vert_len {
        scene_map.push(vec![0; vert_len]);
        let horiz_len = map[i].len();
        for j in 0..horiz_len {
            if i == 0 || j == 0 || i == vert_len - 1 || j == horiz_len - 1 {
                scene_map[i][j] = 0;
            } else {
                let comp_val = map[i][j].clone();
                let left_score = count_until(&map[i][0..j], comp_val, true);
                let right_score = count_until(&map[i][j+1..horiz_len], 
                    comp_val, false);
                let mut vert_vec: Vec<u32> = Vec::new();
                for k in 0..vert_len {
                    vert_vec.push(map[k][j]);
                }
                let top_score = count_until(&vert_vec[0..i], comp_val, true);
                let bottom_score = count_until(
                    &vert_vec[i+1..vert_len], comp_val, false);
                scene_map[i][j] = right_score * left_score * 
                    top_score * bottom_score;
            }
        }
    }
    return scene_map;
}

fn count_until(part: &[u32], cut_off: u32, rev: bool) -> u32 {
    let mut local_vec = part.clone().to_vec();
    if rev {
        local_vec.reverse();
    }
    let mut count = 0;
    for i in local_vec {
        count += 1;
        if i >= cut_off {
            break;
        }
    }
    return count;
} 

fn find_max(map: &Vec<Vec<u32>>) -> u32 {
    let mut maximum = 0;
    for i in 0..map.len() {
        for j in 0..map[i].len() {
            if map[i][j] > maximum {
                maximum = map[i][j];
            }
        }
    }
    return maximum;
}
