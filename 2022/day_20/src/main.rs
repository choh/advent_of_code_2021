use std::env;
use std::fs;
use std::process;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let file_lines: Vec<&str> = file_content.lines().collect();
    let mut sequence: Vec<(usize, i64)> = Vec::with_capacity(file_lines.len());
    for i in 0..file_lines.len() {
        let val = file_lines[i].parse::<i64>().unwrap();
        sequence.push((i, val));
    }

    let mixed = mix(&sequence);
    let zero_idx = mixed.iter().position(|x| x.1 == 0).unwrap() as i64;
    let i1000 = get_index(zero_idx + 1000, mixed.len());
    let i2000 = get_index(zero_idx + 2000, mixed.len());
    let i3000 = get_index(zero_idx + 3000, mixed.len());

    let solution1 = mixed[i1000].1 + mixed[i2000].1 + mixed[i3000].1;
    println!("Solution 1 is {}", solution1);

    let decryption_key = 811589153;
    let mut second_sequence: Vec<(usize, i64)> = Vec::with_capacity(
        sequence.len());
    for item in sequence {
        second_sequence.push((item.0, item.1 as i64 * decryption_key));
    }
    for _ in 0..10 {
        second_sequence = mix(&second_sequence);
    }
    let zero_idx2 = second_sequence.iter()
        .position(|x| x.1 == 0).unwrap() as i64;
    let i21000 = get_index(zero_idx2 + 1000, second_sequence.len());
    let i22000 = get_index(zero_idx2 + 2000, second_sequence.len());
    let i23000 = get_index(zero_idx2 + 3000, second_sequence.len());

    let solution2 = second_sequence[i21000].1 + 
        second_sequence[i22000].1 + 
        second_sequence[i23000].1;
    println!("Solution 2 is {}", solution2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn get_index(pos: i64, len: usize) -> usize {
    let local_len = len as i64;
    let new_idx = ((pos % local_len) + local_len) % local_len;
    return new_idx as usize;
}

fn mix(sequence: &Vec<(usize, i64)>) -> Vec<(usize, i64)> {
    let mut data = sequence.clone();
    let data_len = data.len();
    for i in 0..data_len {
        let next_idx = data.iter().position(|x| x.0 == i).unwrap();
        let to_move = data.remove(next_idx);
        let move_idx = next_idx as i64 + to_move.1;
        let new_idx = get_index(move_idx, data_len - 1);
        data.insert(new_idx, to_move);    
    }
    return data;
}
