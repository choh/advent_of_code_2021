use std::env;
use std::fs;
use std::process;
use regex::Regex;
use std::collections::HashMap;

type Scanner = HashMap<i32, Vec<(i32, i32)>>;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let file_lines: Vec<&str> = file_content.lines().collect();
    let scanners = make_scanners(file_lines);

    let target_row = 2000000;
    //let target_row = 10;
    let ranges = get_locations(&scanners, target_row);
    let intervals = compress_range(&ranges);
    let mut impossible_points = 0;
    for i in (0..intervals.len()).step_by(2) {
        let range_size = intervals[i + 1] - intervals[i];
        impossible_points += range_size;
    }

    println!("Solution 1 is {}", impossible_points);
    
    let upper_limit = 4000000;
    //let upper_limit = 20;
    let mut location: (u64, u64) = (0, 0);
    'limit: for row in 0..=upper_limit {
        let range = get_locations(&scanners, row);
        let compressed = compress_range(&range);
        if compressed.len() == 2 && 
           compressed[0] <= 0 && compressed[1] >= upper_limit {
            continue;
        } else {
            for i in (1..compressed.len()).step_by(2) {
                for j in compressed[i] + 1..=compressed[i + 1] {
                    if j >= 0 && j <= upper_limit {
                        location = (j as u64, row as u64);
                        break 'limit;
                    }
                }
            }
        }        
    }

    let solution2 = location.0 * 4000000 + location.1;
    println!("Solution 2 is {}", solution2);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn make_scanners(lines: Vec<&str>) -> Vec<Scanner> {
    let mut scanners: Vec<Scanner> = Vec::with_capacity(lines.len());
    let coord_rx = Regex::new(r"[xy]=(?P<cr>-{0,1}\d+)").unwrap();
    for line in lines {
        let mut extracted_coords: Vec<i32> = Vec::new();
        for caps in coord_rx.captures_iter(line) {
            extracted_coords.push(caps["cr"].parse::<i32>().unwrap());
        }
        let scanner_coord = (extracted_coords[0], extracted_coords[1]);
        let beacon_coord = (extracted_coords[2], extracted_coords[3]);
        let distance = manhattan_dist(&scanner_coord, &beacon_coord);
        let mut range_container: HashMap<i32, Vec<(i32, i32)>> = HashMap::new();
        for i in -distance..=distance {
            let x_dist = distance - i.abs();
            let extreme_right = (scanner_coord.0 + x_dist,
                                scanner_coord.1 + i);
            let extreme_left = (scanner_coord.0 - x_dist,
                                 scanner_coord.1 + i);
            range_container.insert(extreme_left.1, 
                                  vec![extreme_left, extreme_right]);
        }
        scanners.push(range_container);
    }
    return scanners;
}

fn manhattan_dist(pos1: &(i32, i32), pos2: &(i32, i32)) -> i32 {
    return (pos1.0 - pos2.0).abs() + (pos1.1 - pos2.1).abs();
}

fn get_locations(scanners: &Vec<Scanner>, target_row: i32) -> 
    Vec<(i32, i32)> {
    let mut ranges: Vec<(i32, i32)> = Vec::with_capacity(scanners.len());

    for scanner in scanners {
        let mut at_target: Vec<i32> = Vec::with_capacity(2);
        let to_check = scanner.get(&target_row);
        match to_check {
            Some(value_vec) => {
                for item in value_vec {
                    at_target.push(item.0);
                }
            },
            None => { continue; }
        };
        
        if at_target.len() == 1 {
            ranges.push((at_target[0], at_target[0]));
        } else if at_target.len() == 2 {
            ranges.push((at_target[0], at_target[1]));
        }   
    }
    ranges.sort_by(|a, b| a.0.cmp(&b.0));
    return ranges;
}

fn compress_range(range: &Vec<(i32, i32)>) -> Vec<i32> {
    let mut interval_points: Vec<i32> = Vec::new();

    for point in range {
        if interval_points.len() == 0 {
            interval_points.push(point.0);
            interval_points.push(point.1);
        } else {
            let last_idx = interval_points.len() - 1;
            if point.0 > interval_points[last_idx] {
                interval_points.push(point.0);
                if point.1 > point.0 {
                    interval_points.push(point.1);
                }
            } else if point.1 > interval_points[last_idx] {
                interval_points[last_idx] = point.1;
            }
        }
    }
    return interval_points;
}