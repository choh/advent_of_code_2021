use std::env;
use std::fs;
use std::process;
use std::collections::HashMap;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let mut map: Vec<Vec<char>> = Vec::new();
    let lines: Vec<&str> = file_content.split("\n").collect();
    for line in lines {
        map.push(line.chars().collect());
    }
    let start_coords: (usize, usize) = find_pos(&map, 'S')[0];
    let goal_coords: (usize, usize) = find_pos(&map, 'E')[0];
    let numeric_map = make_numeric(&map);
    let shortest_path = dijkstra(&numeric_map, start_coords, goal_coords);
    let goal_idx = coord_to_id(&numeric_map, &goal_coords.0, &goal_coords.1);
    let distance = shortest_path[goal_idx];
    println!("Solution 1 is {}", distance);
    let possible_starts: Vec<(usize, usize)> = find_pos(&map, 'a');
    let mut distances: Vec<u32> = Vec::new();
    distances.push(distance as u32);
    for this_start in possible_starts {
        let this_dist = dijkstra(&numeric_map, this_start, goal_coords);
        distances.push(this_dist[goal_idx] as u32);
    }
    let smallest_distance = distances.iter().min().unwrap();
    println!("Solution 2 is {}", smallest_distance);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn find_pos(map: &Vec<Vec<char>>, which: char) -> Vec<(usize, usize)> {
    let mut coords: Vec<(usize, usize)> = Vec::new();
    for i in 0..map.len() {
        for j in 0..map[i].len() {
            if map[i][j] == which {
                coords.push((i, j));
            }
        }
    }
    return coords;
}

fn make_numeric(map: &Vec<Vec<char>>) -> Vec<Vec<usize>> {
    static LOWER_LETTERS: [char; 26] = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',  'k', 'l', 'm', 'n', 
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    
    let mut numeric_map: Vec<Vec<usize>> = Vec::new();
    
    for i in 0..map.len() {
        numeric_map.push(Vec::new());
        for j in 0..map[i].len() {
            if map[i][j] == 'S' {
                numeric_map[i].push(0);
            } else if map[i][j] == 'E' {
                numeric_map[i].push(25);
            } else {
                let height = LOWER_LETTERS.iter()
                    .position(|&x| x == map[i][j])
                    .unwrap();
                numeric_map[i].push(height);
            }
        }
    }
    return numeric_map;
}

fn make_adjacency(map: &Vec<Vec<usize>>) ->  HashMap<usize, Vec<usize>> {
    let mut pairs: HashMap<usize, Vec<usize>> = HashMap::new();

    for x in 0..map.len() {
        for y in 0..map[x].len() {
            let mut potential_neighbours: Vec<(usize, usize)> = Vec::new();
            if x > 0 {
                potential_neighbours.push((x - 1, y));
            }
            if x < map.len() - 1 {
                potential_neighbours.push((x + 1, y));
            }
            if y > 0 {
                potential_neighbours.push((x, y - 1));
            }
            if y < map[x].len() - 1{
                potential_neighbours.push((x, y + 1));
            }
            let node_id = coord_to_id(&map, &x, &y);
            let mut neighbours: Vec<usize> = Vec::new();
            for pos in potential_neighbours {
                let diff = map[pos.0][pos.1] as i32 - map[x][y] as i32;
                if diff <= 1 {
                    neighbours.push(coord_to_id(&map, &pos.0, &pos.1));
                }
            }
            pairs.insert(node_id, neighbours);
        }
    }

    return pairs;
}

fn coord_to_id(map: &Vec<Vec<usize>>, y: &usize, x: &usize) -> usize {
    return y * map[0].len() + x;
}

fn dijkstra(map: &Vec<Vec<usize>>,
            start: (usize, usize), end: (usize, usize)) -> Vec<f64> {
    let graph = make_adjacency(&map);
    let graph_size = map.len() * map[0].len();
    let mut distance: Vec<f64> = vec![f64::INFINITY; graph_size];
    distance[coord_to_id(&map, &start.0, &start.1)] = 0.0;
    let mut to_visit: Vec<usize> = (0..graph_size).collect();
    
    while to_visit.len() > 0 {
        let mut next_node: Option<usize> = None;
        let mut select_dist = f64::INFINITY;
        for node in &to_visit {
            if distance[*node] < select_dist {
                select_dist = distance[*node];
                next_node = Some(*node);
            }
        }
        let u = match next_node {
            Some(x) => x,
            None => break
        };
        
        let visit_idx = to_visit.iter().position(|&x| x == u).unwrap();
        to_visit.remove(visit_idx);
        let neighbours = match graph.get(&u) {
            Some(x) => x,
            None => continue
        };
        for neighbour in neighbours {
            let new_dist = distance[u] + 1.0;
            if new_dist < distance[*neighbour] {
                distance[*neighbour] = new_dist;
            }
        }
        if u == coord_to_id(&map, &end.0, &end.1) {
            break;
        }
        
    }
    return distance;

}