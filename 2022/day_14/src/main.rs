use std::env;
use std::fs;
use std::process;
use std::ops::RangeInclusive;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let file_lines: Vec<&str> = file_content.lines().collect();
    let split_lines: Vec<Vec<&str>> = file_lines
        .into_iter()
        .map(|x| x.split(" -> ").collect())
        .collect();
    let rock_vectors = make_rock_vectors(split_lines);
    let extreme_points: (u32, u32, u32, u32) = find_extremes(&rock_vectors);
    let shifted_points = shift_vectors(&rock_vectors, &extreme_points.0);
    let mut map_with_abyss = make_map(&shifted_points, 
                       extreme_points.1 - extreme_points.0 + 1, 
                       extreme_points.3 + 1);
    let mut map_without_abyss = make_map(&rock_vectors, 
        extreme_points.1 * 10,
        extreme_points.3 + 3);
    for x in 0..map_without_abyss[0].len() {
        map_without_abyss[(extreme_points.3 + 2) as usize][x] = '#';
    }
    let start_point: (i32, i32) = (500 - extreme_points.0 as i32, 0);
    let unshifted_start: (i32, i32) = (500, 0);
    let mut count1 = 0;
    loop {
        let sand_in_abyss = move_sand(
            start_point, &mut map_with_abyss, extreme_points.3);
        if sand_in_abyss {
            break;
        }
        count1 += 1;
    }

    let mut count2 = 0;
    loop {
        let cant_move = move_sand(
            unshifted_start, &mut map_without_abyss, 9999);
        if cant_move {
            break;
        }
        count2 += 1;
    }

    println!("Solution 1 is {}", count1);
    println!("Solution 2 is {}", count2 + 1);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn make_rock_vectors(input: Vec<Vec<&str>>) -> Vec<Vec<(u32, u32)>> {
    let mut rock_vectors: Vec<Vec<(u32, u32)>> = Vec::new();
    for line in input {
        let mut this_vector: Vec<(u32, u32)> = Vec::new();
        for i in 0..line.len() {
            let coords: Vec<&str> = line[i].split(",").collect();
            let value0 = coords[0].parse::<u32>().unwrap();
            let value1 = coords[1].parse::<u32>().unwrap();
            this_vector.push((value0, value1));
        }
        rock_vectors.push(this_vector);
    }
    return rock_vectors;
}

fn find_extremes(vectors: &Vec<Vec<(u32, u32)>>) -> (u32, u32, u32, u32) {
    let mut min_x: u32 = 9999;
    let mut max_x: u32 = 0;
    let mut min_y: u32 = 9999;
    let mut max_y: u32 = 0;
    for rock_vector in vectors {
        for coord in rock_vector {
            if coord.0 < min_x {
                min_x = coord.0;
            } else if coord.0 > max_x {
                max_x = coord.0;
            }
            if coord.1 < min_y {
                min_y = coord.1;
            } else if coord.1 > max_y {
                max_y = coord.1;
            }
        }
    }
    return (min_x, max_x, min_y, max_y);
}

fn shift_vectors(vectors: &Vec<Vec<(u32, u32)>>, 
              by_x: &u32) -> Vec<Vec<(u32, u32)>> {
    let mut shifted: Vec<Vec<(u32, u32)>> = Vec::new();
    for line in vectors {
        let mut this_vec: Vec<(u32, u32)> = Vec::new();
        for value in line {
            this_vec.push((value.0 - by_x, value.1));
        }
        shifted.push(this_vec);
    }
    return shifted;
}

fn make_map(vectors: &Vec<Vec<(u32, u32)>>, 
            xdim: u32, ydim: u32) -> Vec<Vec<char>> {
    let mut map: Vec<Vec<char>> = Vec::new();
    for _ in 0..ydim {
        let mut inner: Vec<char> = Vec::new();
        for _ in 0..xdim {
            inner.push(' ');
        }
        map.push(inner);
    }
    for inner_vec in vectors {
        for i in 0..inner_vec.len() - 1{
            map[inner_vec[i].1 as usize][inner_vec[i].0 as usize] = '#';
            let x_start = inner_vec[i].0;
            let x_end = inner_vec[i + 1].0;
            let x_range: RangeInclusive<u32>;
            if x_end < x_start {
                x_range = x_end..=x_start;
            } else {
                x_range = x_start..=x_end;
            }
            for x in x_range {
                let y_start = inner_vec[i].1;
                let y_end = inner_vec[i + 1].1;
                let y_range: RangeInclusive<u32>;
                if y_end < y_start {
                    y_range = y_end..=y_start;
                } else {
                    y_range = y_start..=y_end;
                }
                for y in y_range {
                    map[y as usize][x as usize] = '#';
                }
            }
        }
    }
    return map;
}

fn move_sand(start: (i32, i32), 
             map: &mut Vec<Vec<char>>,
             abyss_start: u32) -> bool {
    let mut coord: (i32, i32) = start;
    let in_abyss: bool;
    loop {
        if coord.1 >= abyss_start as i32 {
            println!("Reached abyss");
            in_abyss = true;
            break;
        }
        if map[(coord.1 + 1) as usize][coord.0 as usize] == ' ' {
            coord = (coord.0, coord.1 + 1);
        } else {
            if coord.0 - 1 < 0 {
                println!("Reached left border");
                in_abyss = true;
                break;
            }
            if map[(coord.1 + 1) as usize][(coord.0 - 1) as usize] == ' ' {
                coord = (coord.0 - 1, coord.1 + 1);
            } else if map[
                (coord.1 + 1) as usize][(coord.0 + 1) as usize] == ' ' {
                coord = (coord.0 + 1, coord.1 + 1);
            } else {
                map[coord.1 as usize][coord.0 as usize] = 'o';
                if coord == start {
                    println!("Reached start");
                    in_abyss = true;
                } else {
                    in_abyss = false;
                }       
                break;
            }
        }
    }
    return in_abyss;
}