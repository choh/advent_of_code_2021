use std::env;
use std::fs;
use std::process;
use std::collections::HashMap;

struct Directory {
    files: HashMap<String, u32>,
    children: Vec<Directory>,
    total_size: u32
}

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let lines: Vec<&str> = file_content.split("\n").collect();
    let (directory_tree, _) = parse_dir(&lines, 0);
    let total_size = get_size(&directory_tree, 100000);
    println!("Solution 1 is {}", total_size);
    let file_system_size = 70000000;
    let update_size = 30000000;
    let unused_space = file_system_size - directory_tree.total_size;
    let needed_space = update_size - unused_space;
    let smallest_to_delete = get_smallest_to_delete(
        &directory_tree, needed_space, file_system_size);
    println!("Solution 2 is {}", smallest_to_delete);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn parse_dir(lines: &Vec<&str>, 
             offset: usize) -> (Directory, usize) {
    let mut directory_tree = Directory {
        children: Vec::new(),
        files: HashMap::new(),
        total_size: 0
    };
    let max_len = lines.len();
    let mut keep_reading = true;
    let mut i = offset;
    while keep_reading {
        let tokens: Vec<&str> = lines[i].split(" ").collect();
        if tokens[0] == "$" {
            if tokens[1] == "cd" && tokens[2] != ".." {
                let (sub_dir, lines_read) = parse_dir(
                    &lines[i+1..max_len].to_vec(), 
                    0);
                directory_tree.total_size += &sub_dir.total_size;
                directory_tree.children.push(sub_dir);
                i = i + lines_read;
            } else if tokens[1] == "cd" && tokens[2] == ".." {
                keep_reading = false;
            }
        } else if tokens[0].parse::<u32>().is_ok() {
            let this_size = tokens[0].parse::<u32>().unwrap_or(0);
            directory_tree.files
                .insert(tokens[1].to_string(), this_size);
            directory_tree.total_size += this_size;
        }
        i = i + 1;
        if i >= max_len {
            keep_reading = false;
        }
    }
    return (directory_tree, i);
}

fn get_size(directory_tree: &Directory, limit: u32) -> u32 {
    let mut size_sum = 0;
    if directory_tree.children.len() > 0 {
        for child in &directory_tree.children {
            size_sum += get_size(child, limit);
        }
    }
    if directory_tree.total_size <= limit {
        size_sum += directory_tree.total_size;
    }
    return size_sum;
}

fn get_smallest_to_delete(directory_tree: &Directory, 
                          needed: u32, closest: u32) -> u32 {
    let mut use_size: u32 = closest;
    if directory_tree.children.len() > 0 {
        for child in &directory_tree.children {
            use_size = get_smallest_to_delete(child, needed, use_size);
        }
    }
    if directory_tree.total_size > needed {
        if directory_tree.total_size < use_size {
            return directory_tree.total_size;
        }
    }        
    return use_size;
}
