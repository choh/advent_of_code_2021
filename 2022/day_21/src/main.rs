use std::env;
use std::fs;
use std::process;
use std::collections::HashMap;
use regex::Regex;

fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let file_lines: Vec<&str> = file_content.lines().collect();
    let monkeys: Vec<Vec<&str>> = file_lines
        .iter()
        .map(|x| x.split(": ").collect())
        .collect();
    let mut numbers: HashMap<String, String> = HashMap::new();
    let mut tasks: HashMap<String, (String, String, String)> = HashMap::new();
    let number_regex = Regex::new(r"\d+").unwrap();

    for entry in monkeys {
        if number_regex.is_match(entry[1]) {
            numbers.insert(entry[0].to_string(), entry[1].to_string());
        } else {
            let task_vec: Vec<&str> = entry[1].split(" ").collect();
            tasks.insert(entry[0].to_string(), 
                         (task_vec[0].to_string(), 
                         task_vec[1].to_string(), 
                         task_vec[2].to_string())
            );
        }
    }
    while tasks.len() > 0 {
        (numbers, tasks) = update_numbers(numbers, tasks);
    }
    println!("Solution 1 is {}", numbers.get(&String::from("root")).unwrap());
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn update_numbers(number_map: HashMap<String, String>,
    target_map: HashMap<String, (String, String, String)>) ->
    (HashMap<String, String>, HashMap<String, (String, String, String)>) {
    let mut target = target_map.clone();
    let mut known = number_map.clone();
    let number_regex = Regex::new(r"\d+").unwrap();

    for (key, value) in &target_map {        
        if number_regex.is_match(&value.0) && 
            number_regex.is_match(&value.2) {
                let result = do_math(&value);
                known.insert(key.to_string(), result);
                target.remove(&*key);
        } else {
            if known.contains_key(&value.0) {
                target.insert(key.to_string(),
                    (known.get(&value.0).unwrap().clone(), value.1.clone(), value.2.clone())
                );
            }
            if known.contains_key(&value.2) {
                target.insert(key.to_string(),
                    (value.0.clone(), value.1.clone(), known.get(&value.2).unwrap().clone())
                );
            }
        }
    }
   
    return (known, target);
}

fn do_math(input: &(String, String, String)) -> String {
    let num1 = input.0.parse::<u64>().unwrap();
    let num2 = input.2.parse::<u64>().unwrap();
    let result: u64 = match input.1.as_str() {
        "+" => num1 + num2,
        "-" => num1 - num2,
        "*" => num1 * num2,
        "/" => num1 / num2,
        _ => panic!()
    };
    return result.to_string();
}