use std::env;
use std::fs;
use std::process;

#[derive(Clone, PartialEq)]
struct Rock {
    borders: Vec<(usize, usize)>,
    halt: bool
}


fn main() {
    let use_file = get_args();
    let file_content = read_file(&use_file);
    let wind: Vec<char> = file_content.chars().collect();
    let wind_max_idx = wind.len() - 1;
    let mut map: Vec<Vec<u8>> = vec![vec![0; 4]; 7];
    
    let mut rock_idx: usize = 0;
    let mut height: usize = 0;
    let mut wind_idx: usize = 0;

    for _ in 0..2022 {
        rock_idx += 1;
        if rock_idx > 5 {
            rock_idx = 1;
        }
        let mut this_rock = get_rock(rock_idx, height + 3);
        let yborder = &this_rock.borders
            .iter()
            .map(|&x| x.1)
            .collect::<Vec<usize>>();
        let rock_max_y = yborder.iter().max().unwrap();
        if rock_max_y >= &map[0].len() {
            for i in 0..map.len() {
                map[i].append(&mut vec![0; 4]);
            }
        }
        
        loop {
            let new_pos_side = move_rock_sideways(&this_rock, wind[wind_idx]);
            wind_idx += 1;
            if wind_idx > wind_max_idx {
                wind_idx = 0;
            }
            let mut collision_side = false;
            'collision_side: for coord in &new_pos_side.borders {
                collision_side = map[coord.0][coord.1] == 1;
                if collision_side {
                    break 'collision_side;
                }
            }
            if !collision_side {
                this_rock = new_pos_side;
            }
            
            let mut collision_down = false;
            let new_pos_down = move_rock_downwards(&this_rock);
            'collision_down: for coord in &new_pos_down.borders {
                collision_down = map[coord.0][coord.1] == 1;
                if collision_down {
                    break 'collision_down;
                }
            }
            
            if this_rock == new_pos_down {
                collision_down = true;
            }

            if !collision_down {
                this_rock = new_pos_down;
            } else {
                this_rock.halt = true;
            }
            
                
            if this_rock.halt {
                for point in this_rock.borders {
                    map[point.0][point.1] = 1;
                }
                let mut heights: Vec<usize> = Vec::new();
                for stack in &map {
                    heights.push(
                        stack.iter().rposition(|&x| x == 1).unwrap_or(0));
                }
                height = *heights.iter().max().unwrap_or(&0) + 1;
                break; 
            }
        }
    }
    println!("Solution 1 is {}", height);
}

fn get_args() -> String {
    let args: Vec<String> = env::args().collect();
    let file_name = String::from(&args[1]);
    return file_name;
}

fn read_file(file_name: &String) -> String {
    let content = fs::read_to_string(file_name).unwrap_or(String::from(""));
    if content == "" {
        println!("Could not read file.");
        process::exit(1);
    }
    return String::from(&content);
}

fn get_rock(which: usize, at_height: usize) -> Rock {
    return match which {
        1 => {
            // @@@@
            Rock {
                borders: vec![(2, at_height), (3, at_height), 
                              (4, at_height), (5, at_height)],
                halt: false
            }
        },
        2 => {
            //  @
            // @@@
            //  @
            Rock {
                borders: vec![(2, at_height + 1), (3, at_height + 2), 
                              (3, at_height + 1), (3, at_height), 
                              (4, at_height + 1)],
                halt: false
            }
        },
        3 => {
            //   @
            //   @
            // @@@
            Rock {
                borders: vec![(2, at_height), (3, at_height),
                              (4, at_height), (4, at_height + 1), 
                              (4, at_height + 2)],
                halt: false
            }
        },
        4 => {
            // @
            // @
            // @
            // @
            Rock {
                borders: vec![(2, at_height + 3), (2, at_height + 2),
                              (2, at_height + 1), (2, at_height)],
                halt: false
            }
        },
        5 => {
            // @@
            // @@
            Rock {
                borders: vec![(2, at_height), (2, at_height + 1),
                              (3, at_height), (3, at_height + 1)],
                halt: false,
            }
        },
        _ => {
            println!("Unknown rock.");
            panic!();
        }
    };
}

fn move_rock_sideways(use_rock: &Rock, direction: char) -> Rock {
    let mut rock = use_rock.clone();
    return match direction {
        '>' => {
            if rock.borders.iter().any(|x| x.0 == 6) {
                rock
            } else {
                rock.borders = rock.borders
                    .iter()
                    .map(|x| (x.0 + 1, x.1))
                    .collect();
                rock
            }
        },
        '<' => {
            if rock.borders.iter().any(|x| x.0 == 0) {
                rock
            } else {
                rock.borders = rock.borders
                    .iter()
                    .map(|x| (x.0 - 1, x.1))
                    .collect();
                rock
            }
        },
        _ => {
            println!("Unknown direction.");
            panic!();
        }
    }
}

fn move_rock_downwards(use_rock: &Rock) -> Rock {
    let mut rock = use_rock.clone();
    return if rock.borders.iter().any(|x| x.1 == 0) {
        rock
    } else {
        rock.borders = rock.borders
            .iter()
            .map(|x| (x.0, x.1 - 1))
            .collect();
        rock
    }
}