#! /usr/bin/env python3
import re
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    input_lines = [line.replace(".", "_") for line in input_lines]
    symbol_locations = [find_matches(line, "symbol") for line in input_lines]
    digit_locations = [find_matches(line, "digit") for line in input_lines]
    to_consider = find_numbers(digit_locations, symbol_locations, input_lines)
    solution_1 = get_sum(to_consider)
    print(f"Solution for part 1 is {solution_1}")
    gear_locations = [find_matches(line, "gear") for line in input_lines]
    gears = get_gears(digit_locations, gear_locations, input_lines)
    solution_2 = gear_ratio_sum(gears)
    print(f"Solution for part 2 is {solution_2}")


def find_matches(input_line, what):
    if what == "symbol":
        symbol_pattern = re.compile(r"\W")
    elif what == "digit":
        symbol_pattern = re.compile(r"\d+")
    elif what == "gear":
        symbol_pattern = re.compile(r"\*")
    symbol_matches = []
    this_match = symbol_pattern.finditer(input_line)
    for m in this_match:
        symbol_matches.append(m.span())
    if len(symbol_matches) == 0:
        symbol_matches.append((-1, -1))
    return symbol_matches


def find_numbers(digits, symbols, digit_lines):
    found_numbers = []
    for num, row in enumerate(digits):
        for this_match in row:
            if this_match[0] == -1:
                continue
            range_lower = 0 if num == 0 else num - 1
            range_upper = num + 1 if num == (len(digits) - 1) else num + 2
            for symbol_match in symbols[range_lower:range_upper]:
                for symbol_loc in symbol_match:
                    if symbol_loc[0] == -1:
                        continue
                    start = symbol_loc[0] - 1
                    stop = symbol_loc[1] + 1
                    if max(this_match[0], start) < min(this_match[1], stop):
                        found_numbers.append(digit_lines[num][this_match[0]:this_match[1]])
    return found_numbers

def get_gears(digits, gears, digit_lines):
    found_gears = []
    for num, row in enumerate(gears):
        for this_match in row:
            if this_match[0] == -1:
                continue
            range_lower = 0 if num == 0 else num - 1
            range_upper = num + 1 if num == (len(digits) - 1) else num + 2
            this_gear_numbers = []
            for i, digit_match in enumerate(digits[range_lower:range_upper]):
                for digit_loc in digit_match:
                    if digit_loc[0] == -1:
                        continue
                    start = digit_loc[0] - 1
                    stop = digit_loc[1] + 1
                    if num == 0:
                        offset = 1 if i == 1 else 0
                    else:
                        if i == 0:
                            offset = -1
                        elif i == 2:
                            offset = 1
                        else:
                            offset = 0
                    if max(this_match[0], start) < min(this_match[1], stop):
                        this_gear_numbers.append(digit_lines[num+offset][start+1:stop-1])
            if len(this_gear_numbers) == 2:
                found_gears.append(this_gear_numbers)
    return found_gears

def get_sum(number_strings):
    this_sum = 0
    for x in number_strings:
        this_sum += int(x)
    return this_sum

def gear_ratio_sum(gear_list):
    this_sum = 0
    for gear in gear_list:
        this_sum += int(gear[0]) * int(gear[1])
    return this_sum
    

if __name__ == "__main__":
    main()