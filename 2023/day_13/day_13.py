#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [line.rstrip() for line in f]
    patterns = split_patterns(input_lines)
    hsym = [horizontal_symmetry(pattern) for pattern in patterns]
    vsym = [vertical_symmetry(pattern) for pattern in patterns]
    solution_1 = sum(hsym) + sum(vsym)
    print(f"The solution for part 1 is {solution_1}.")
    hsym_smudge = [horizontal_symmetry_with_smudge(pattern) for pattern in patterns]
    vsym_smudge = [vertical_symmetry_with_smudge(pattern) for pattern in patterns]
    solution_2 = sum(hsym_smudge) + sum(vsym_smudge)
    print(f"The solution for part 2 is {solution_2}.")


def split_patterns(rows):
    pattern_list = []
    pattern_list.append([])
    counter = 0
    for row in rows:
        if len(row) == 0:
            counter += 1
            pattern_list.append([])
            continue
        else:
            pattern_list[counter].append(list(row))
    return pattern_list


def horizontal_symmetry(pattern):
    for i, row in enumerate(pattern[:-1]):
        if row == pattern[i + 1]:
            equality = []
            for x in range(i):
                comp = i * 2 + 1 - x
                if comp < len(pattern):
                    equality.append(pattern[x] == pattern[comp])
            if all(equality):
                return (i + 1) * 100
    return 0


def horizontal_symmetry_with_smudge(pattern):
    for i, _ in enumerate(pattern[:-1]):
        not_equal = []
        for x in range(i + 1):
            comp = i * 2 + 1 - x
            if comp < len(pattern):
                item_comparison = []
                for j, _ in enumerate(pattern[x]):
                    item_comparison.append(pattern[x][j] != pattern[comp][j])
                not_equal.append(sum(item_comparison))
        if sum(not_equal) == 1:
            return (i + 1) * 100
    return 0

def get_col(pattern, idx):
    return [row[idx] for row in pattern]

def vertical_symmetry(pattern):
    row_len = len(pattern[0])
    for i in range(row_len - 1):
        if get_col(pattern, i) == get_col(pattern, i + 1):
            equality = []
            for x in range(i):
                comp = i * 2 + 1 - x
                if comp < row_len:
                    equality.append(get_col(pattern, x) == get_col(pattern, comp))
            if all(equality):
                return i + 1
    return 0

def vertical_symmetry_with_smudge(pattern):
    row_len = len(pattern[0])
    for i in range(row_len - 1):
        not_equal = []
        for x in range(i + 1):
            comp = i * 2 + 1 - x
            if comp < row_len:
                item_comparison = []
                this_col = get_col(pattern, x)
                comp_col = get_col(pattern, comp)
                for j, _ in enumerate(this_col):
                    item_comparison.append(this_col[j] != comp_col[j])
                not_equal.append(sum(item_comparison))
        if sum(not_equal) == 1:
            return i + 1
    return 0


if __name__ == "__main__":
    main()
    