#! /usr/bin/env python3
from pathlib import Path
import sys

def main():
    input_file = Path(sys.argv[1])
    with input_file.open(encoding="utf8") as f:
        input_lines = [list(line.rstrip()) for line in f]
    final_steps, loop, init_tile = find_loop(input_lines)
    solution_1 = int(min(final_steps) / 2)
    print(f"The solution for part 1 is {solution_1}")
    start = loop[0]
    input_lines[start[0]][start[1]] = init_tile
    grid_with_loop = mark_loop(input_lines, loop)
    solution_2 = find_inside(grid_with_loop)
    print(f"The solution for part 2 is {solution_2}")

def find_start(grid):
    for y, row in enumerate(grid):
        if "S" in row:
            x = row.index("S")
            break
    return (y, x, 0, 0)

def find_loop(grid):
    start = find_start(grid)
    options = ["F", "J", "7", "L"]
    final_steps = []
    for opt in options:
        path = []
        this_grid = grid
        if opt == "F" or opt == "7":
            mov = (start[0], start[1], 1, 0)
        else:
            mov = (start[0], start[1], -1, 0)
        this_grid[start[0]][start[1]] = opt
        steps = 0
        while True:
            try:
                path.append((mov[0], mov[1]))
                mov = move(this_grid, mov)
                steps += 1
            except ValueError:
                break
            except IndexError:
                break
            if mov[0] == start[0] and mov[1] == start[1]:
                final_steps.append(steps)
                final_path = path
                break
    return (final_steps, final_path, opt)

def move(grid, mov):
    current_mov = grid[mov[0]][mov[1]]
    yvel, xvel = mov[2], mov[3]
    if current_mov == "|":
        newmov = (mov[0] + yvel, mov[1], yvel, xvel)
    elif current_mov == "-":
        newmov = (mov[0], mov[1] + xvel, 0, xvel)
    elif current_mov == "L":
        if xvel == -1:
            newmov = (mov[0] - 1, mov[1], -1, 0)
        else:
            newmov = (mov[0], mov[1] + 1, 0, 1)
    elif current_mov == "J":
        if xvel == 1:
            newmov = (mov[0] - 1, mov[1], -1, 0)
        else:
            newmov = (mov[0], mov[1] - 1, 0, -1)
    elif current_mov == "7":
        if xvel == 1:
            newmov = (mov[0] + 1, mov[1], 1, 0)
        else:
            newmov = (mov[0], mov[1] -1, 0, -1)
    elif current_mov == "F":
        if xvel == -1:
            newmov = (mov[0] + 1, mov[1], 1, 0)
        else:
            newmov = (mov[0], mov[1] + 1, 0, 1)
    elif current_mov == "S":
        newmov = (mov[0] + xvel, mov[1] + yvel, xvel, yvel)
    else:
        raise ValueError("Impossible to move")

    if newmov[3] == 1 and grid[newmov[0]][newmov[1]] in ["|", "F", "L"]:
        raise ValueError("Illegal move")
    elif newmov[3] == -1 and grid[newmov[0]][newmov[1]] in ["|", "J", "7"]:
        raise ValueError("Illegal move")
    elif newmov[2] == 1 and grid[newmov[0]][newmov[1]] in ["-", "F", "7"]:
        raise ValueError("Illegal move")
    elif newmov[2] == -1 and grid[newmov[0]][newmov[1]] in ["-", "L", "J"]:
        raise ValueError("Illegal move")

    if mov[0] < 0 or mov[1] < 0:
        raise ValueError("Left grid")
    return newmov

def mark_loop(grid, path):
    for y, row in enumerate(grid):
        for x, tile in enumerate(row):
            grid[y][x] = { "tile": tile, "loop": False }
    for coord in path:
        grid[coord[0]][coord[1]]["loop"] = True
    return grid

def find_inside(grid):
    inside_count = 0
    for row in grid:
        prev_tile = ""
        inside = False
        for tile in row:
            if tile["loop"]:
                if tile["tile"] == "|":
                    inside = not inside
                elif tile["tile"] == "7":
                    if prev_tile == "L":
                        inside = not inside
                elif tile["tile"] == "J":
                    if prev_tile == "F":
                        inside = not inside
                if tile["tile"] != "-":
                    prev_tile = tile["tile"]
            else:
                if inside:
                    inside_count += 1
    return inside_count


if __name__ == "__main__":
    main()