use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let nums: Vec<Vec<i64>> = file_content.iter().map(|x| parse_line(x)).collect();
    let mut result1: i64 = 0;
    let mut result2: i64 = 0;
    for row in &nums {
        let res1 = is_solvable(&row[0], &row[1..], false);
        if res1 {
            result1 += row[0];
        }
        let res2 = is_solvable(&row[0], &row[1..], true);
        if res2 {
            result2 += row[0];
        }
    }

    println!("Solution 1 is {}", result1);
    println!("Solution 2 is {}", result2);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn parse_line(line: &String) -> Vec<i64> {
    let clean_line = line.replace(":", "");
    let split_line: Vec<&str> = clean_line.split_whitespace().collect();
    let res = split_line.iter().map(|x| x.parse::<i64>().unwrap()).collect();
    res
}

fn is_solvable(target: &i64, values: &[i64], use_concat: bool) -> bool {
    if values.len() == 1 && values[0] == *target {
        return true
    }
    if values.len() < 1 {
        return false
    }
    if values.len() == 1 && values[0] != *target {
        return false
    }
 
    let first_mult = values[0] * values[1];
    let mut mult_values = values[2..].to_vec();
    mult_values.insert(0, first_mult);

    let first_add = values[0] + values[1];
    let mut add_values = values[2..].to_vec();
    add_values.insert(0, first_add);

    let can_concat = use_concat && values.len() > 1;
    let mut values_concat: Vec<i64> = Vec::new();
    if can_concat {
        let concat = format!("{}{}", values[0], values[1]).parse::<i64>().unwrap();
        values_concat = values[2..].to_vec();
        values_concat.insert(0, concat);
    } 

    if can_concat {
        return is_solvable(&target, &mult_values[..], use_concat) || 
               is_solvable(&target, &add_values[..], use_concat) || 
               is_solvable(&target, &values_concat[..], use_concat)
    } else {
        return is_solvable(&target, &mult_values[..], use_concat) || 
               is_solvable(&target, &add_values[..], use_concat)
    }
}