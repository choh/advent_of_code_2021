use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let split_lines: Vec<Vec<&str>> = file_content
        .iter()
        .map(|line| line.split_ascii_whitespace().collect::<Vec<&str>>())
        .collect();
    let col_0 = extract_numbers(&split_lines, 0);
    let col_1 = extract_numbers(&split_lines, 1);
    let mut distances: Vec<i32> = Vec::new();
    for i in 0..col_0.len() {
        let result = col_0[i] - col_1[i];
        distances.push(result.abs());
    }
    let solution_1: i32 = distances.iter().sum();
    println!("Solution 1 is: {}", solution_1);

    let mut products: Vec<i32> = Vec::new();
    for i in 0..col_0.len() {
        let result = col_0[i] * count_item(col_0[i], &col_1[..]);
        products.push(result);
    }
    let solution_2: i32 = products.iter().sum();
    println!("Solution 2 is: {}", solution_2);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn extract_numbers(data: &Vec<Vec<&str>>, idx: usize) -> Vec<i32> {
    let mut numbers: Vec<i32> = Vec::new();
    for item in data {
        let this_val: i32 = item[idx].parse::<i32>().unwrap();
        numbers.push(this_val);
    }
    numbers.sort();
    numbers
}

fn count_item(value: i32, data: &[i32]) -> i32 {
    data.iter().filter(|x| **x == value).count() as i32
}
