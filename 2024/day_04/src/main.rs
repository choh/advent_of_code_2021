use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let char_lines: Vec<Vec<char>> = file_content.into_iter().map(|x| x.chars().collect()).collect();
    let solution_1 = find_word_in_grid(&char_lines, String::from("XMAS"), 'y');
    println!("Solution 1 is: {}", solution_1);
    let solution_2 = find_words_in_grid_xshape(&char_lines, String::from("MAS"), 'y');
    println!("Solution 2 is: {}", solution_2);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn pad_grid(raw_grid: &Vec<Vec<char>>, by: &usize, with: char) -> Vec<Vec<char>> {
    let vert_pad = vec![with; raw_grid.len() + (by - 1) * 2];
    let mut grid: Vec<Vec<char>> = Vec::new();
    for _ in 0..by-1 {
        grid.push(vert_pad.clone());
    }
    for row in raw_grid {
        grid.push([vec![with; by-1], row.clone(), vec![with; by-1]].concat());
    }
    for _ in 0..by-1 {
        grid.push(vert_pad.clone());
    }
    grid
}

fn find_word_in_grid(raw_grid: &Vec<Vec<char>>, word: String, pad_char: char) -> u32 {
    let word_chars: Vec<char> = word.chars().collect();
    let word_length = word_chars.len();
    let grid = pad_grid(raw_grid, &word_length, pad_char);
    let rows = grid.len();
    let cols = grid[0].len();
    let mut occurrences: u32 = 0;

    for y in (word_length-1)..(rows-word_length+1) {
        for x in (word_length-1)..(cols-word_length+1) {
            if grid[y][x] == word_chars[0] {
                let mut word_vec: Vec<String> = Vec::new();
                // forward
                let fw: &[char] = &grid[y][x..x+word_length];
                word_vec.push(fw.into_iter().collect());
                // backward
                let bw: &[char] = &grid[y][x+1-word_length..=x];
                word_vec.push(bw.into_iter().rev().collect());
                // vert down
                let vd: &[char] = &grid[y..y+word_length].iter().map(|v| v[x]).collect::<Vec<char>>();
                word_vec.push(vd.into_iter().collect());
                // vert up
                let vu: &[char] = &grid[y+1-word_length..=y].iter().map(|v| v[x]).collect::<Vec<char>>();
                word_vec.push(vu.into_iter().rev().collect());
                // diag down left
                let mut dl: Vec<char> = Vec::new();
                for i in 0..word_length {
                    dl.push(grid[y+i][x-i]);
                }
                word_vec.push(dl.into_iter().collect());
                // diag down right
                let mut dr: Vec<char> = Vec::new();
                for i in 0..word_length {
                    dr.push(grid[y+i][x+i]);
                }
                word_vec.push(dr.into_iter().collect());
                // diag up left
                let mut ul: Vec<char> = Vec::new();
                for i in 0..word_length {
                    ul.push(grid[y-i][x-i]);
                }
                word_vec.push(ul.into_iter().collect());
                // diag up right
                let mut ur: Vec<char> = Vec::new();
                for i in 0..word_length {
                    ur.push(grid[y-i][x+i]);
                }
                word_vec.push(ur.into_iter().collect());
                for this_word in word_vec {
                    if this_word == word {
                        occurrences += 1;
                    }
                }
            }
        }
    }
    occurrences
}

fn find_words_in_grid_xshape(raw_grid: &Vec<Vec<char>>, word: String, pad_char: char) -> u32 {
    let word_chars: Vec<char> = word.chars().collect();
    let word_length = word_chars.len();
    let grid = pad_grid(raw_grid, &word_length, pad_char);
    let rows = grid.len();
    let cols = grid[0].len();
    let mut occurrences: u32 = 0;
    let midpoint = (word_length - 1) / 2;
    let anchor = word_chars[midpoint];
    let reversed_word: String = word_chars.iter().rev().collect();

    for y in (word_length-1)..(rows-word_length+1) {
        for x in (word_length-1)..(cols-word_length+1) {
            if grid[y][x] == anchor {
                let mut word_vec: Vec<String> = Vec::new();
                // top left to bottom right
                let mut dl: Vec<char> = Vec::new();
                for i in 0..word_length {
                    dl.push(grid[(y-midpoint)+i][(x-midpoint)+i]);
                }
                word_vec.push(dl.into_iter().collect());
                // bottom left to top right
                let mut dr: Vec<char> = Vec::new();
                for i in 0..word_length {
                    dr.push(grid[(y+midpoint)-i][(x-midpoint)+i]);
                }
                word_vec.push(dr.into_iter().collect());
                let this_match: bool = word_vec
                    .into_iter()
                    .map(|x| x == word || x == reversed_word)
                    .collect::<Vec<bool>>()
                    .iter()
                    .all(|&b| b);
                if this_match {
                    occurrences += 1;
                }
            }
        }
    }
    occurrences
}