use std::fs::File;
use std::env;
use std::io::prelude::*;
use std::io::BufReader;

fn main() {
    let input_args: Vec<String> = env::args().collect();
    let file_content = read_file(&input_args[1]);
    let mut sum_of_valid_lines: u32 = 0;
    for line in &file_content {
        if is_line_valid(&line, None).0 {
            sum_of_valid_lines += 1;
        }
    }
    println!("Solution 1 is: {}", sum_of_valid_lines);
    let mut sum_of_valid_lines_dampened: u32 = 0;
    'outer: for line in &file_content {
        let check_result = is_line_valid(&line, None);
        if check_result.0 {
            sum_of_valid_lines_dampened += 1;
        } else {
            let check_idx: Vec<usize>;
            if check_result.1 == 0 {
                check_idx = vec![0, 1];
            } else if check_result.1 == line.len() - 1 {
                check_idx = vec![line.len() - 2, line.len() - 1];
            } else {
                check_idx = vec![check_result.1 - 1, check_result.1, check_result.1 + 1];
            }
            for idx in check_idx {
                let second_check = is_line_valid(&line, Some(idx));
                if second_check.0 {
                    sum_of_valid_lines_dampened += 1;
                    continue 'outer;
                }
            }
        }
    }
    println!("Solution 2 is: {}", sum_of_valid_lines_dampened);
}

fn read_file(file_name: &String) -> Vec<String> {
    let file_handle = File::open(file_name).unwrap();
    let file_reader = BufReader::new(file_handle);
    let file_content: Vec<String> = file_reader.lines().map(|x| x.unwrap()).collect();
    file_content
}

fn is_line_valid(line: &String, remove_idx: Option<usize>) -> (bool, usize) {
    let split_line: Vec<&str> = line.split_ascii_whitespace().collect();
    let mut numbers_vec: Vec<i32> = split_line.iter().map(|x| x.parse::<i32>().unwrap()).collect();
    if remove_idx.is_some() {
        numbers_vec.remove(remove_idx.unwrap());
    }
    let first_diff = numbers_vec[0] - numbers_vec[1];
    if !check_diff(first_diff) {
        return (false, 0)
    }
    let increasing = first_diff < 0;
    for i in 0..numbers_vec.len() - 1 {
        let this_diff = numbers_vec[i] - numbers_vec[i + 1];
        if !check_diff(this_diff) {
            return (false, i)
        }
        if increasing && this_diff > 0 {
            return (false, i)
        } else if !increasing && this_diff < 0 {
            return (false, i)
        }
    }
    (true, 0)
}

fn check_diff(diff: i32) -> bool {
    if diff == 0 {
        return false
    } else if diff.abs() > 3 {
        return false 
    }
    true
}